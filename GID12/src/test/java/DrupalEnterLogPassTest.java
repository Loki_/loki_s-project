import org.junit.Assert;
import org.junit.Test;
import prm.DrupalBaseTest;
import prm.DrupalEnterLogin;
import prm.DrupalLogin;

/**
 * Created by User on 20-Aug-16.
 */
public class DrupalEnterLogPassTest extends DrupalBaseTest{
    public static String expectedResult = "Log out";

    @Test
    public void drupalTest01() throws InterruptedException {
        DrupalLogin logPass = new DrupalLogin();
        logPass.getSingIm(chdriver, "oa.org.ua", "12345Q");
        DrupalEnterLogin loginPassed = new DrupalEnterLogin();
        String singInText = loginPassed.enterLogin(chdriver);

        Assert.assertEquals("DrupalEnterLogPass", expectedResult, singInText);


    }
}
