import org.junit.Assert;
import org.junit.Test;
import prm.DriverClose;
import prm.DrupalBaseTest;
import prm.DrupalErrorText;
import prm.DrupalLogin;


/**
 * Created by User on 20-Aug-16.
 */
public class DrupalPassTest extends DrupalBaseTest{

    public static String expectedResult = "Error message\nPassword field is required.\nSorry, unrecognized username or password. Have you forgotten your password?";

    @Test
    public void drupalTest01() {
        DrupalLogin logPass = new DrupalLogin();
        logPass.getSingIm(chdriver, "oa.org.ua", "");
        DrupalErrorText error = new DrupalErrorText();
        String errorMessage = error.ErrorText(chdriver);

        Assert.assertEquals("DrupalPass", expectedResult, errorMessage);

    }
}
