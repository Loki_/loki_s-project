import org.junit.Assert;
import org.junit.Test;
import prm.DrupalBaseTest;
import prm.DrupalErrorText;
import prm.DrupalLogin;

/**
 * Created by User on 20-Aug-16.
 */
public class DrupalLoginTest extends DrupalBaseTest{
    public static String expectedResult = "Error message\nUsername or email field is required.";

        @Test
        public void drupalTest01() {
        DrupalLogin logPass = new DrupalLogin();
        logPass.getSingIm(chdriver, "", "12345Q");
        DrupalErrorText error = new DrupalErrorText();
        String errorMessage = error.ErrorText(chdriver);

        Assert.assertEquals("DrupalPass", expectedResult, errorMessage);

        }
    }

