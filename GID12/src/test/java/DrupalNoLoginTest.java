import org.junit.Assert;
import org.junit.Test;
import prm.DriverClose;
import prm.DrupalBaseTest;
import prm.DrupalErrorText;
import prm.DrupalLogin;

/**
 * Created by User on 21-Aug-16.
 */
public class DrupalNoLoginTest extends DrupalBaseTest {
    public static String expecredResult = "Error message\nSorry, unrecognized username or password. Have you forgotten your password?";

    @Test
    public void DrupalTestNoLogin() {
        DrupalLogin logPass = new DrupalLogin();
        logPass.getSingIm(chdriver, "oa.org.ua", "123456Q");
        DrupalErrorText error = new DrupalErrorText();
        String errorMessage = error.ErrorText(chdriver);

        Assert.assertEquals("DrupalNoLogin", expecredResult, errorMessage);
        DriverClose bay = new DriverClose();
        bay.winClose(chdriver);
    }
}
