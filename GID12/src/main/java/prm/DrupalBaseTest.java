package prm;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by User on 23-Aug-16.
 */

public class DrupalBaseTest {
    public static WebDriver chdriver;

    @BeforeClass
    public static void getLogPage() throws InterruptedException {
        chdriver = new ChromeDriver();
        chdriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        chdriver.get("https://www.drupal.org/user/login?destination=home");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    @AfterClass
    public static void winClose() {
        chdriver.close();
        chdriver.quit();
    }
}