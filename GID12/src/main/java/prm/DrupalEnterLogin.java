package prm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by User on 25-Aug-16.
 */
public class DrupalEnterLogin {
    public static String enterLogin(WebDriver chdriver) throws InterruptedException {
        WebElement rdButton = DrupalBaseTest.chdriver.findElement(By.xpath("//a[@href='#block-system-user-menu']"));
        rdButton.click();
        Thread.sleep(1000);
        String singInText = DrupalBaseTest.chdriver.findElement(By.xpath("//a[contains(text(), 'Log out')]")).getText();
        return singInText;
    }
}
