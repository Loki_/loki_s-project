package prm;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by User on 23-Aug-16.
 */
public class DrupalErrorText {
    public static String ErrorText(WebDriver chdriver) {
        WebElement errorBlock = chdriver.findElement(By.xpath("//div[@id='content']/div[@class='messages error']"));
        String str11 = errorBlock.getText();
        return str11;
    }
}
